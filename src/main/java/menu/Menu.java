package menu;

import java.util.Scanner;

public class Menu {
    private StringManager stringManager = new StringManager();

    public void showMenu() {
        String userInput;
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println();
            System.out.println("Choose from the following list");
            System.out.println();
            System.out.println("1 -> Add a new string");
            System.out.println("2 -> Show all strings occurrences");
            System.out.println("3 -> Show the average of the string input");
            System.out.println("4 -> Quit");
            System.out.print("\n>");

            userInput = scanner.next();

            if ("1".equals(userInput)) {
                stringManager.readInput();
            } else if ("2".equals(userInput)) {
                if (stringManager.getInput() == null) {
                    System.out.println("Please first enter a string!");
                    System.out.println();
                    showMenu();
                } else {
                    stringManager.numberOfStringOccurrence();
                }

            } else if ("3".equals(userInput)) {
                if (stringManager.getInput() == null) {
                    System.out.println("Please first enter a string!");
                    System.out.println();
                    showMenu();
                } else {
                    stringManager.averageStringLength();
                }
            } else if ("4".equals(userInput)) {
                System.out.println("The program will shut down!");
                System.exit(0);
            } else {
                System.out.println("Invalid choice. Read the options again!");
            }
        }
    }
}
