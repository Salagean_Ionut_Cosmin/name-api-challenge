package menu;

import java.util.*;

import static java.util.stream.Collectors.toMap;

public class StringManager {

    private final Scanner scanner = new Scanner(System.in);

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    private String input;

    public void readInput() {
        System.out.println("Enter the sentence that you want to store!");
        System.out.println();

        input = scanner.nextLine();
    }

    public void numberOfStringOccurrence() {
        String[] temp = input.split(" ");
        int counter;
        Map<String, Integer> stringOccurrence = new HashMap<>();

        for (int i = 0; i < temp.length; i++) {
            if (stringOccurrence.containsKey(temp[i])) {
                counter = stringOccurrence.get(temp[i]);
                stringOccurrence.put(temp[i], ++counter);
            } else {
                stringOccurrence.put(temp[i], 1);
            }
        }

        System.out.println("For the current input each string occurred: ");

        Map<String, Integer> sorted = stringOccurrence
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));

        for (Map.Entry<String, Integer> entry : sorted.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue() + " times");
        }
    }

    public void averageStringLength() {
        String[] temp = input.split(" ");
        double average = 0;
        for (int i = 0; i < temp.length; i++) {
            int indexLength = temp[i].length();
            average = (average + indexLength);

        }
        average = average / temp.length;
        System.out.println("The average of the string length is: " + average);
        System.out.println();
    }
}
